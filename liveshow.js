class LiveShow extends Live {
    constructor(canvas, width, height) {
        super(width, height);
        this.points = [];
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.canvasData = this.ctx.getImageData(0, 0, width, height);
        this.rect = canvas.getBoundingClientRect();
    };

    _listener(e) {
        let x = e.pageX - parseInt(this.rect.left);
        let y = e.pageY - parseInt(this.rect.top);
        this.points.push({x:x, y:y});
        this.ctx.fillStyle = 'white';
        this.ctx.fillRect(x, y, 1, 1);
        if(this.points.length > 50) {
            this.canvas.removeEventListener('mousedown', this._listener);
            super.init(this.points);
            this.run();
        };
    };

    init() {
        this.canvas.addEventListener('mousedown', this._listener.bind(this), false);
    };

    run() {
        setInterval(() => {
            if(!super.run()) return;
            for(let x = 0; x < this.width ; x++) {
                for(let y = 0; y < this.height; y++) {
                    if(this.a[x][y]) {
                        this.ctx.fillStyle = 'gold';
                        this.ctx.fillRect(x, y, 1, 1);
                    } else {
                        this.ctx.fillStyle = '#112233';
                        this.ctx.clearRect(x, y, 1, 1);
                    }
                };
            };
        }, 1);
    };
};