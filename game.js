// (EN) Any software development by your technical requirements
// (RU) Разработка любого софта по вашему ТЗ
// -------------------------------------------------------
// Maxim +7-905-26-939-26 <pinktronik@gmail.com> 
"use strict";

let main = function() {
    let width = 1000;
    let height = 400;

    let canvas = document.createElement("canvas");
    document.getElementById('arena').appendChild(canvas);
    //canvas = document.getElementsByName('canvas')[0];

    canvas.width = width;
    canvas.height = height;
    if (canvas.getContext) {
        let game = new LiveShow(canvas, width, height);
        game.init();
    } else {
        console.log("canvas.getContext == false !!!");
    }
};

document.addEventListener("DOMContentLoaded", main);