// (EN) Any software development by your technical requirements
// (RU) Разработка любого софта по вашему ТЗ
// -------------------------------------------------------
// Maxim +7-905-26-939-26 <pinktronik@gmail.com> 
"use strict";

// Игра Жизнь
// ==========
// конструктор устанавливает размер поля
// init(array) - устанавливает начатьное расположение бактерий
// run() - выполняет один проход по полю, возвращает true если жизнь продолжается, иначе false

class Live {
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.alive = 0;
        this.prev = [0,0];
        this.rise = 0;
        this.counter = 0;
        this.a = [];
        for(let x=0; x <= this.width; x++) {
            this.a[x] = [];
            for(let y=0; y <= this.height; y++) {
                this.a[x][y] = 0;
            };
        };
    };

    init(points) {
        points.map((p) => { this.a[p.x][p.y] = 1;} );
    };

    run() {
        this.counter++;
        this.prev[1] = this.prev[0];
        this.prev[0] = this.alive;
        this.alive = 0;
        for(let y=0; y <= this.height; y++) {
            let yp = (y == 0)? this.height : y - 1 ;
            let yn = (y == this.height)? 0 : y + 1 ;
            for(let x=0; x <= this.width; x++) {
                let xp = (x == 0)? this.width : x - 1 ;
                let xn = (x == this.width)? 0 : x + 1 ;
                let sum = this.a[xp][yp] + this.a[x][yp] + this.a[xn][yp]
                        + this.a[xp][y]        +           this.a[xn][y]
                        + this.a[xp][yn] + this.a[x][yn] + this.a[xn][yn];
                if (sum == 3) {
                    this.a[x][y] = 1;
                } else if (sum < 2 || sum > 3 ) {
                    this.a[x][y] = 0;
                };
                this.alive += this.a[x][y];
            };
        };
        this.rise = this.prev[0] - this.alive;
        return(!(this.prev[1] == this.alive && this.prev[0] == this.alive) && this.alive != 0);
    };
};

/*
function selftest() {
    let live = new Live(800, 600);
    live.init([ {x:1, y:1}, {x:1, y:2}, {x:1, y:3}, {x:2, y:1}, {x:3, y:1} ]); 
    while(live.run()) { 
        console.log(live.counter+': '+live.alive);
    };
    console.log(live.counter+': '+live.alive);
};

selftest();
*/